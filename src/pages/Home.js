import Banner from "../components/Banner";
import { Container, Col, Row } from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react';
import Highlights from "../components/Highlights";
import FeaturedProducts from "../components/FeaturedProducts";
import UserContext from '../UserContext'
// import CourseCard from "../components/CourseCard";


export default function Home() {
    const { user } = useContext(UserContext);
    const data ={
    title: 'Let the game begin !',
    content:  `Looking for old games? Don't worry, we got you covered! The game is not over til it's over, player!`,
    destination: "/products",
    label: `Choose now!`
    }


    return (
    <Container fluid className="min-vh-100" >

      <Banner data={data} />
      <Row>
      <Col className ="p-5" >
      {!user.isAdmin ? <FeaturedProducts  /> : null}
      
      <Highlights />
      </Col>
      </Row>
    </Container>
    )
}