import Banner from '../components/Banner';
import {Container} from 'react-bootstrap'


export default function Error() {

  const data = {
    title: "404 - Page Not Found",
    content: "The Page you are looking for cannot be found",
    destination: "/",
    label: "Back Home"
  }

  return (
    <Container fluid className="vh-100">
         <Banner data={data}/>
    </Container>
  )
};