import {useState, useEffect, Fragment} from 'react';
import logo from './logo.svg';
import { Route, Routes } from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom'
import { Container } from 'react-bootstrap';
import { UserProvider } from './UserContext';
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer'
import ProductView from './pages/ProductView';
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import Home from './pages/Home'
import ManageOrders from './pages/ManageOrders'
import AddProducts from './pages/AddProducts'
import ManageCart from './pages/ManageCart'
import Error from './pages/Error';
import './App.css';


function App() {
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }
  useEffect(() => {

      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`
          }
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          // Set the user states values with the user details upon successful login.
          if (typeof data._id !== "undefined") {

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

          // Else set the user states to the initial values
          } else {

            setUser({
              id: null,
              isAdmin: null
            });

          }

        })

        }, []);

  useEffect(() =>{
    console.log(user);
    console.log(localStorage);
  }, [user])

    return (
        <Fragment>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
          <link
            href="https://fonts.googleapis.com/css2?family=Offside&display=swap"
            rel="stylesheet"
          />

          <UserProvider value={{ user, setUser, unsetUser }}>
            <Router>
              <AppNavbar />
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/products" element={<Products />} />
                <Route path="/products/:productId" element={<ProductView user={user} />} />
                <Route path="/addProduct" element={<AddProducts />} />
                <Route path="/register" element={<Register />} />
                <Route path="/manageOrder" element={<ManageOrders />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/manageCart" element={<ManageCart user={user} />} />
                <Route path="*" element={<Error />} />
                {/*<Route path="/profile" element={<Profile />} />*/}
              </Routes>
              <Footer />
            </Router>
          </UserProvider>
        </Fragment>
      );

  }

export default App;
