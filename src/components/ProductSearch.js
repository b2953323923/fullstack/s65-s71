import React, { useState } from 'react';
import ProductCard from './ProductCard';

const ProductSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [showResults, setShowResults] = useState(false); // New state to control result visibility

  const handleSearch = async () => {
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/search`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ productName: searchQuery })
      });
      const data = await response.json();
      setSearchResults(data);
      setShowResults(true); // Show results after search is complete
    } catch (error) {
      console.error('Error searching for product:', error);
    }
  };

  return (
    <div className="pt-5 container">
      <h2>Product Search</h2>
      <div className="form-group">
        <label htmlFor="productName">Product Name:</label>
        <input
          type="text"
          id="productName"
          className="form-control"
          value={searchQuery}
          onChange={event => setSearchQuery(event.target.value)}
        />
      </div>
      <button className="btn btn-primary my-4" onClick={handleSearch}>
        Search
      </button>
      {showResults && ( // Render the search results or "No results found" message
        <>
          {searchResults.length === 0 ? (
            <p>No results found.</p>
          ) : (
            <>
              <h3>Search Results:</h3>
              <ul>
                {searchResults.map(product => (
                  <ProductCard productProp={product} key={product._id} />
                ))}
              </ul>
            </>
          )}
        </>
      )}
    </div>
  );
};

export default ProductSearch;