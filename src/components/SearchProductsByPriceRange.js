import React, { useState } from 'react';
import ProductCard from './ProductCard';

const ProductSearch = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [error, setError] = useState(null);
  const [searchPerformed, setSearchPerformed] = useState(false); // New state to indicate if search is performed

  const handleSearch = async () => {
    // Parse the prices and set them as numbers using setMinPrice and setMaxPrice
    const parsedMinPrice = minPrice !== '' ? parseFloat(minPrice) : '';
    const parsedMaxPrice = maxPrice !== '' ? parseFloat(maxPrice) : '';

    // Ensure that minPrice is less than or equal to maxPrice
    if (parsedMinPrice !== '' && parsedMaxPrice !== '' && parsedMinPrice > parsedMaxPrice) {
      alert('Min price cannot be greater than max price.');
      return;
    }

    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/products/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ minPrice: parsedMinPrice, maxPrice: parsedMaxPrice }),
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      const data = await response.json();
      setSearchResults(data.products);
      setError(null); // Clear any previous errors

      // Update searchPerformed to true after the search is complete
      setSearchPerformed(true);
    } catch (error) {
      console.error('Error searching for products:', error);
      setError('An error occurred while fetching the data.');
    }
  };

  return (
    <div className="container mt-4">
      <h3>Search Products by Price Range</h3>
      <div className="">
        <div className="row col-md-4 pb-3">
          <div className="form-group">
            <label>Min Price:</label>
            <input
              type="number"
              className="form-control"
              value={minPrice}
              onChange={(e) => setMinPrice(e.target.value)}
            />
          </div>
        </div>
        <div className="row col-md-4">
          <div className="form-group pb-5">
            <label>Max Price:</label>
            <input
              type="number"
              className="form-control"
              value={maxPrice}
              onChange={(e) => setMaxPrice(e.target.value)}
            />
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-md-12">
          <button className="btn btn-primary" onClick={handleSearch}>
            Search
          </button>
        </div>
      </div>
      <div className="row mt-4">
        <div className="col-md-12">
          {searchPerformed ? ( // Show the "No results found" message only if search is performed
            searchResults.length > 0 ? (
              <div>
                <h4>Search Results:</h4>
                <ul>
                  {searchResults.map((product) => (
                    <ProductCard productProp={product} key={product._id} />
                  ))}
                </ul>
              </div>
            ) : (
              <p>No results found.</p>
            )
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default ProductSearch;