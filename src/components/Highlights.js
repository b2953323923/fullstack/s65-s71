import {Row, Col, Card, Container} from 'react-bootstrap';

export default function Highlights(){
    return (
       <Container fluid>
        <Row className='mt-3 mb-3 px-5'>
            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>###</Card.Title>
                        <Card.Text>
                           Some quick example text to build on the card title and make up the
                                                   bulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>###</Card.Title>
                        <Card.Text>
                        Some quick example text to build on the card title and make up the
                        bulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

            <Col xs={12} md={4}>
                <Card className='cardHiglight p-3'>
                    <Card.Body>
                        <Card.Title>###</Card.Title>
                        <Card.Text>
                        Some quick example text to build on the card title and make up the
                        bulk of the card's content.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>

        </Row>
       </Container>
    )
}