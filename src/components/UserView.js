import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from './ProductCard';
import ProductSearch from './ProductSearch';
import SearchProductsByPriceRange from './SearchProductsByPriceRange';

export default function UserView({ productsData }) {
  const [products, setProducts] = useState([]);
  const [currentSearchOption, setCurrentSearchOption] = useState('PRODUCT_SEARCH');

  useEffect(() => {
    const activeProducts = productsData.filter((product) => product.isActive === true);
    setProducts(activeProducts);
  }, [productsData]);

  const handleSearchOptionChange = (event) => {
    setCurrentSearchOption(event.target.value);
  };

  return (
    <Container className="py-5">
      <h2 className="mb-4 text-center">Products</h2>
      <div className="text-center mb-4">
        <select value={currentSearchOption} onChange={handleSearchOptionChange}>
          <option value="PRODUCT_SEARCH">Product Search</option>
          <option value="SEARCH_BY_PRICE_RANGE">Search by Price Range</option>
        </select>
      </div>
      {currentSearchOption === 'PRODUCT_SEARCH' && <ProductSearch />}
      {currentSearchOption === 'SEARCH_BY_PRICE_RANGE' && <SearchProductsByPriceRange />}
    </Container>
  );
}