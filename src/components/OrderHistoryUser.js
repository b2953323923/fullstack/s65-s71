import React, { useEffect, useState } from 'react';
import { Card, Row, Col } from 'react-bootstrap';

export default function OrderHistoryUser({ ordersData }) {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    if (Array.isArray(ordersData)) {
      const ordersArr = ordersData.map((order) => (
        <Col key={order._id} lg={6}>
          <Card className="mb-3">
            <Card.Body>
              <Card.Title>Order ID: {order._id}</Card.Title>
              <Card.Text>User ID: {order.userId}</Card.Text>
              <Card.Text>Product Name: {order.products[0].productName}</Card.Text>
              <Card.Text>No. of Products: {order.products.length}</Card.Text>
              <Card.Text>Total Amount: PhP {order.totalAmount}</Card.Text>
              <Card.Text>Purchased Date: {new Date(order.purchasedOn).toLocaleString()}</Card.Text>
            </Card.Body>
          </Card>
        </Col>
      ));
      setOrders(ordersArr);
    }
  }, [ordersData]);

  return (
    <>
      <h2 className="text-center p-3">Your Order History</h2>
      {Array.isArray(ordersData) && ordersData.length > 0 ? (
        <Row>{orders}</Row>
      ) : (
        <p className="text-center">No orders found.</p>
      )}
    </>
  );
}
