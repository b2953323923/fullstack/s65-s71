import { Col, Card, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Product(props) {
	console.log(props)
	const { breakPoint, data } = props

	const { _id, name, description, price, imgUrl } = data

	return (

		<Col xs={12} md={breakPoint} className="p-5">
			<Card className="cardHighlight bg-dark text-white">
			{imgUrl && (
			    <div className ="image-container">
			            <Card.Img src={imgUrl} alt={name} className="product-image p-3"/>
			    </div>
			    )}
				<Card.Body>
					<Card.Title className="text-center">
					<Link className ="fw-bold text-decoration-none text-white" to={`/products/${_id}`}>{name}</Link>
					</Card.Title>
					<Card.Text className ="text-center">{description}</Card.Text>
				</Card.Body>

				<Card.Footer>
					<h5 className="text-center text-white">&#8369;{price}</h5>
					<Link to={`/products/${_id}`} className="btn btn-danger d-block">Details</Link>
				</Card.Footer>
			</Card>

		</Col>

	)
}