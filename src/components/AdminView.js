import { useState, useEffect } from 'react';
import { Table, Row, Container, Col } from 'react-bootstrap';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct'
export default function AdminView({productsData, fetchProduct}) {

  
  const [products, setProducts] = useState([]);


  useEffect(() => {
    const productsArr = productsData.map(product => {
      console.log(product)
      return (
        <tr key={product._id} >
          <td>{product._id}</td>
          <td>{product.name}</td>
          <td>{product.description}</td>
          <td> &#8369;{product.price}</td>
          
          <td className={product.isActive ? "text-success" : "text-danger"}>
            {product.isActive ? "Available" : "Unavailable"}
          </td>
          <td>
          <img src={product.imgUrl} alt={product.name} style={{width:'100px'}}/>
          </td>
          <td><EditProduct product={product._id} fetchProduct={fetchProduct}/></td>  
          <td><ArchiveProduct productId={product._id} isActive={product.isActive} fetchProduct={fetchProduct}/></td>
        </tr>
        )
    })  

    setProducts(productsArr)

  }, [productsData, fetchProduct])

  return(
    <Container fluid >
    <Row>
    <Col>
      <h1 className="text-center my-4"> Admin Dashboard</h1>

      <Table striped bordered hover responsive>
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Availability</th>
            <th className = 'text-center'>Image</th>
            <th colSpan="2">Actions</th>
          </tr>
        </thead>

        <tbody>
          {products}
        </tbody>
      </Table> 
      </Col>
      </Row> 
    </Container>

  )
}