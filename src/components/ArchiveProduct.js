import Swal from 'sweetalert2';

export default function ArchiveProduct({ productId, isActive, fetchProduct }) {
    
    // Function for archive toggle
    const archiveToggle = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archiveProduct`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Course Successfully Updated',
          });
          fetchProduct();
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again',
          });
          fetchProduct();
        }
      });
  };

  const activateToggle = (e) => {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activateProduct`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Success!',
            icon: 'success',
            text: 'Product Successfully Updated'
          })
          fetchProduct();
        } else {
          Swal.fire({
            title: 'Error!',
            icon: 'error',
            text: 'Please try again'
          })
          fetchProduct();
        }
      });
  };

  return (
    <>
      {isActive ? (
        <button className="btn btn-danger" onClick={archiveToggle}>
          Deactivate
        </button>
      ) : (
        <>
          <button className="btn btn-success" onClick={activateToggle}>
            Activate
          </button>
        </>
      )}
    </>
  );
  
}
