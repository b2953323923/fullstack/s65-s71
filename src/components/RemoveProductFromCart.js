import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const RemoveProductModal = ({ show, onHide, product, onRemoveProduct }) => {
  const handleRemove = () => {
    onRemoveProduct(product.productId);
    onHide();
  };

  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>Remove Product</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>Are you sure you want to remove this product from your cart?</p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}className ="px-3">
          Cancel
        </Button>
        <Button variant="danger" onClick={handleRemove}>
          Remove
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default RemoveProductModal;