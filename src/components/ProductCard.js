// import { useState, useEffect } from 'react';

import {Row, Col, Card, Button, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../App.css'

export default function ProductCard({productProp}) {

   

    const { _id, name, description, price, imgUrl } = productProp;

    
    

    return (
        <Card id='productComponent1' className="cardHighlight p-3 h-100 bg-dark   text-light">
        <Container className ="h-100">
        <Row>
        <Col xs={12} >
        {imgUrl && (
            <div className ="image-container">
                    <Card.Img src={imgUrl} alt={name} className="product-image my-5"/>
            </div>
            )}
        
        </Col> 
        <Col xs={12}>  
            <Card.Body>
           
                
                             
                        <Card.Title className ="mb-3 ">{name}</Card.Title>
                        <Card.Subtitle className ="mb-3">Description:</Card.Subtitle>
                        <Card.Text className ="mb-3">{description}</Card.Text>
                        <Card.Subtitle className ="mb-3">Price:</Card.Subtitle>
                        <Card.Text className="text-light mb-3"> &#8369; {price}</Card.Text>
                        {/*<Card.Text>Enrollees: {count}</Card.Text>*/}
                       
                        <Button className="view-details bg-danger" as={Link} to={`/products/${_id}`} variant="primary">View Details</Button>
                    
               
                   
            </Card.Body>
                     </Col>
                </Row>
            </Container>

        </Card>
    )
};