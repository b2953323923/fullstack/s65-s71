import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { FaFacebookF, FaTwitterSquare, FaInstagram } from 'react-icons/fa';

const Footer = () => {
  return (
    <footer className="bg-dark text-light ">
      <Container fluid>
        <Row className="p-3">
          <Col md={12} className="text-center">
            <p className="mb-0">
              © {new Date().getFullYear()} EtherealXgeekS. All rights reserved.
              <span className="mx-2">|</span>
              <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer" className="mr-3 text-light">
                <FaFacebookF />
              </a>
              <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer" className="mr-3 text-light px-3">
                <FaTwitterSquare />
              </a>
              <a href="https://www.instagram.com/" target="_blank" rel="noopener noreferrer" className="text-light">
                <FaInstagram />
              </a>
            </p>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;