import { Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'
import { useContext } from 'react'
export default function Banner ({data}) {
  const { title, content, destination, label} = data;
  const { user } = useContext(UserContext);
  return (
   <Row>
      <Col className="p-5  text-center banner-col">
        {/* Background Image */}
        <div className="banner-background"></div>
        
        {/* Dark Gradient Overlay */}
        <div className="banner-gradient-overlay"></div>

        {/* Content */}
        <div className="banner-content">
          <h1>{title}</h1>
          <p>{content}</p>
          {!user.isAdmin ? (
            <Link className="btn btn-danger" to={destination}>
              {label}
            </Link>
          ) : null}
        </div>
      </Col>
    </Row>
  );
}